# OpiBus Coding Challenge

OpiBus Home Assessment Test

This is a coding challenge for a software developer position at OpiBus, The solutions to questions has been tackled in javascript language.

## Getting started

To get started you will need nodejs and npm installed in your local machine.
To install node follow: [Nodejs](https://nodejs.org/en)

After install confirm that you have node installed
```
node -v
```
You should get a number e.g. `v16.0.0` indicating the version number of installed node


## Grab this code

Get a copy of this code repo into your local machine

```
git clone https://gitlab.com/Delink-D/opibus-coding-challenge.git
cd opibus-coding-challenge
```

## Folder structure

Each folder holds the implementation for each question as shown below

    .
    ├── .git
    ├── question-1               # question 1 directory
    │   └── app.js               # question 1 executable file
    ├── question-2               # question 2 directory
    │   └── app.js               # question 2 executable file
    ├── question-3               # question 3 directory
    │   └── app.js               # question 3 executable file
    └── README.md

## Installations and Running

The code is written in pure javascript, To run the code open terminal at the root folder then run the following commands:

- To run Question 1: `node question-1/app.js`
    - The sample arrays are already added as console logs, results of each array will be printed on the terminal 
    ```
      // console log
      console.log(highestAvg([3, 4])); // ans = 3.5
      console.log(highestAvg([2, 3, 1, 5])); // ans = 3
      console.log(highestAvg([2, 3, 4, 1, 5])); // ans = 3.5
      console.log(highestAvg([20, 40, 50, 10, 50])); // ans = 45
    ```
- To run Question 2: `node question-2/app.js`
    - The sample arrays are already added as console logs, results of each array will be printed on the terminal
    ```
    // console
    const availableString = 'Bike will be available for all the bookings';
    const notAvailableString = 'Bike will NOT be available for all the bookings';
    console.log(findAvailabilityBooking([[1, 4], [2, 5], [7, 9]]) ? availableString : notAvailableString);
    console.log(findAvailabilityBooking([[6, 7], [2, 4], [8, 12]]) ? availableString : notAvailableString);
    console.log(findAvailabilityBooking([[4, 5], [2, 3], [3, 6]]) ? availableString : notAvailableString);
    console.log(findAvailabilityBooking([[7, 10], [11, 15], [15, 16]]) ? availableString : notAvailableString);
    ```
- To run Question 3: `node question-3/app.js`
   - For this question classes to support additional drive modes have been added, running the above command will display different driving modes on the terminal
   ```
   // Initialize classes and start driving
  const driveEchoMode = new DriveECHOMode();
  const driveSportMode = new DriveSportMode();
  const driveOffRoadMode = new DriveOffRoadMode();
  console.log(driveEchoMode.drive());
  console.log(driveSportMode.drive());
  console.log(driveOffRoadMode.drive());
   ``` 