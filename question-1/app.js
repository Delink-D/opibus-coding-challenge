/**
 * Question 1.
 * Given an array containing the rate of discharge of an electric vehicle,
 * which 2 contiguous rate of discharge elements in the array will have the
 * highest average. Create a function that will accept the array as a
 * parameter and return the highest average between 2 contiguous elements.
 * 
 * Assume the values of the array will always be positive
 * Input: [2, 3, 4, 1, 5]
 * Output: 3.5
 * 
 * Explanation: Subarray with maximum sum is [3, 4] / 2 = 3.5
 */

/**
 * Function get an array with rate of discharge and return return the highest average between 2 contiguous values
 * @param {Array} array containing the rate of discharge of an electric vehicle
 * @returns max of 2 contiguous rate
 */
function findHighestAvg(array, k = 2) {
  let groupedSum = 0, maxAvg = 0;

  // loop through list of items
  array.forEach((item, index) => {
    groupedSum += item;  // add elements together

    // only proceed when we have the hit the required item size
    if (index >= k - 1) {
      maxAvg = Math.max(maxAvg, groupedSum / k); // get the max average between the previous and the current avg
      groupedSum -= array[index - (k - 1)]; // subtract the 1st element of the grouped pair
    }
  });

  return maxAvg;
}

// console log
console.log(findHighestAvg([3, 4])); // ans = 3.5
console.log(findHighestAvg([2, 3, 1, 5])); // ans = 3
console.log(findHighestAvg([2, 3, 4, 1, 5])); // ans = 3.5
console.log(findHighestAvg([20, 40, 50, 10, 50])); // ans = 45
