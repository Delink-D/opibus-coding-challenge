/**
 * Question 2.
 * We would like to start Electric bike booking service where our E-bike
 * clients can book a bike for a period of time. Given an array of intervals
 * represents "N" bookings, find out if a particular bike will be available
 * for all the booking?. Create a function that accepts an array of bookings
 * and returns a boolean.
 * 
 * Example 1: Bookings: [[1,4], [2,5], [7,9]]
 * Output: false

 * Explanation: Since [1,4] and [2,5] overlap, the bike cannot be available for both of these bookings.
 *
 * Example 2: Bookings: [[6,7], [2,4], [8,12]]
 * Output: true
 * 
 * Explanation: None of the bookings overlap, therefore the bike is available for all of them.
 * 
 * Example 3: Bookings: [[4,5], [2,3], [3,6]]
 * Output: false
 * 
 * Explanation: Since [4,5] and [3,6] overlap, the bike cannot be available for both of these bookings.
 */

/**
 * Function find if a bike is available for booking, from provided list of intervals
 * @param {Array} array of time intervals for booking
 * @returns {Boolean} true | false
 */
function findAvailabilityBooking(array) {
  let mergedArray = [];

  // loop through the list of arrays to create one long array of intervals
  array.forEach((arr, i) => {
    // for each of the array, get all the values between the intervals as items of the array
    let rangeIntervals = range(arr[0], arr[arr.length - 1]);

    // add all the arrays into one long array of time intervals
    mergedArray = mergedArray.concat(rangeIntervals);
  });

  // remove duplicates values, overlapping values
  const uniqMergedArray = mergedArray.filter((val, index) => mergedArray.indexOf(val) === index);

  // check if the long array is equal to the uniq array
  // if its same, that means there was no overlapping time slots
  // if its not the same it means some overlapping values have been removed
  return uniqMergedArray.length === mergedArray.length;
}

/**
 * Function return an array of values, given start and end value
 * @param {*} start value of the array
 * @param {*} end value of the array
 * @returns array of range
 */
function range(start = 0, end) {
  const results = [];

  // check if end value is missing
  if (!end) {
    end = start; // set end as the start value
    start = 0;  // set start value to 0
  }

  // loop through start to the end creating an array of values
  for (var i = start; i <= end; i += 1) {
    results.push(i);
  }

  // return the final array of values from range
  return results;
}

// console
const availableString = 'Bike will be available for all the bookings';
const notAvailableString = 'Bike will NOT be available for all the bookings';
console.log(findAvailabilityBooking([[1, 4], [2, 5], [7, 9]]) ? availableString : notAvailableString);  // ans = false
console.log(findAvailabilityBooking([[6, 7], [2, 4], [8, 12]]) ? availableString : notAvailableString); // ans = true
console.log(findAvailabilityBooking([[4, 5], [2, 3], [3, 6]]) ? availableString : notAvailableString);  // ans = false
console.log(findAvailabilityBooking([[7, 10], [11, 15], [15, 16]]) ? availableString : notAvailableString); // ans = false
console.log(findAvailabilityBooking([[9, 17]]) ? availableString : notAvailableString); // ans = true
