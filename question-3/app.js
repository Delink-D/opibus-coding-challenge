/**
 * Question 3.
 * In our code base we have a DriverControl class that controls the
 * driving behaviour of our vehicles. It is currently very rigid and only supports
 * one driving style. We would like to add different driving
 * modes/behaviours where the user can decide which driving style they would like. Explain how you
 * would extend the class below to add ECO, SPORT, OFF-ROAD driving modes. You
 * can create simple classes that don't really have implementations but just
 * class names and method names, or you could use UML diagrams.
 */

/** Class driver control */
class DriverControl {
  driveMode = 'Normal';
  constructor(driveMode) { this.driveMode = driveMode }

  drive() { return `You are driving our Mercedes-Benz in ${this.driveMode} mode` }
}

/**
 * To add support for ECO, SPORT, OFF-ROAD driving modes
 * We would need to add other classes that extend from the parent driverControl
 */

/** Class drive ECHO mode */
class DriveECHOMode extends DriverControl {
  constructor() {
    super('ECHO');
  }

  // TODO: Implement methods for echo mode
}

/** Class drive SPORT mode */
class DriveSportMode extends DriverControl {
  constructor() {
    super('SPORT');
  }

  // TODO: Implement methods for sport mode
}

/** Class drive OFF-ROAD mode */
class DriveOffRoadMode extends DriverControl {
  constructor() {
    super('OFF-ROAD');
  }

  // TODO: Implement methods for off-road mode
}

// Initialize classes and start driving
const driveEchoMode = new DriveECHOMode();
const driveSportMode = new DriveSportMode();
const driveOffRoadMode = new DriveOffRoadMode();
console.log(driveEchoMode.drive());
console.log(driveSportMode.drive());
console.log(driveOffRoadMode.drive());